package com.wtcweb.mobileapps2017.jacobmcallister;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.wtcweb.mobileapps2017.R;

public class BtnCounter extends AppCompatActivity {

    Integer countTracker = 0;
    Button btnCounter;
    TextView lblCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btn_counter);

        lblCounter = (TextView)findViewById(R.id.lblCounter);

        btnCounter = (Button) findViewById(R.id.btnCounter);
        btnCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                countTracker++;
                //Toast.makeText(MainActivity.this, Integer.toString(countTracker), Toast.LENGTH_SHORT).show();
                lblCounter.setText(Integer.toString(countTracker));



            }
        });

    }
}

